import java.util.*;
import java.util.List;
import java.awt.*;
public class SimulationManager implements Runnable {
	public int timeLimit;
	public int maxProcessingTime;
	public int minProcessingTime;
	public int numberOfServers;
	public int numberOfClients;
	private Scheduler scheduler;
	private SimulationFrame frame;
	private List<Task> generatedTasks;
	
	public SimulationManager(){
		frame = new SimulationFrame(this);
	}
	private void generateNRandomTasks(){//genereaza taskuri random care sa fie puse in coada
		Random r = new Random();
		for (int i=0; i<numberOfClients;i++){
			generatedTasks.add(new Task(r.nextInt(timeLimit), r.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime,  i));
		}
	}
	@Override
	public void run(){
		scheduler = new Scheduler(numberOfServers, numberOfClients);
		generatedTasks = new ArrayList<Task>();
		generateNRandomTasks();
		Collections.sort(generatedTasks);//sorteaza lista in functie de timpul de sosire
		int currentTime=0;
		int contor = 0;
		while(currentTime<timeLimit){
			while (contor < numberOfClients && generatedTasks.get(contor).getArrivalTime() <= currentTime){//pune clientii care au ajuns in cozi
				scheduler.dispatchTask(generatedTasks.get(contor));
				System.out.println("Task-ul" + generatedTasks.get(contor).id + " a ajuns");
				contor++;
			}
			Task[][] taskuri = new Task[numberOfServers][];//facem o matrice de taskuri pentru afisare
			for (int i = 0; i < numberOfServers; i++)
				taskuri[i] = scheduler.getServers().get(i).getTasks();
			frame.displayData(taskuri);//chem functia de afisare
			currentTime++;
			try {
				Thread.sleep(1000);//il pun sa astepte o secunda
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args){
		SimulationManager gen=new SimulationManager();
	}
	public void numberOfClients(int parseInt) {
		numberOfClients=parseInt;
		
	}
	public void minProcessingTime(int parseInt) {
		minProcessingTime=parseInt;
		
	}
	public void numberOfServers(int parseInt) {
		numberOfServers=parseInt;
		
	}
	public void timp(int parseInt) {
		timeLimit=parseInt;
		
	}
	public void maxProcessingTime(int parseInt) {
		maxProcessingTime=parseInt;
		
	}
	
}
