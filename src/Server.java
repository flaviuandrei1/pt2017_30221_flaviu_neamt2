import java.lang.Thread;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
	private BlockingQueue<Task> tasks;
	private AtomicInteger waitingPeriod;
	private int dimensiune;
	public Server(int dimensiune){
		waitingPeriod= new AtomicInteger();
		this.dimensiune = dimensiune;
		tasks = new ArrayBlockingQueue<Task>(dimensiune);
		
	}
	public void addTask(Task newTask){//pune un task si schimba timpul de astapetare
		tasks.add(newTask);
		newTask.setWaitingTime(waitingPeriod.get());//seteaza la task timpul de asteptare al cozii
		waitingPeriod.addAndGet(newTask.getProcessingPeriod());//schimba timpul de asteptare
	}
	public void run(){
		while(true){
			try {
				Task t = tasks.peek();//pune in t primul element din coada fara sa il scoata
				if(t != null){//este null doar daca este goala lista
					Thread.sleep(t.getProcessingPeriod()*1000);//face threadul sa doarma pentru cat timp este procesat taskul
					waitingPeriod.addAndGet(-t.getProcessingPeriod());//schimba timpul de asteptare
					System.out.println("Taskul"+t.id+"a iesit de la coada asta la timpul"+t.getFinishTime());
					tasks.take();//clientul il scoate din coada
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	public Task[] getTasks(){//face din coada un vector pentru afisare
		Task[] task = new Task[tasks.size()];
		int i = 0;
		for (Task t : tasks){
			task[i] = t;
			i++;
		}
		return task;
	}
	public int getWaitingTime(){
		return waitingPeriod.get();
	}
}
